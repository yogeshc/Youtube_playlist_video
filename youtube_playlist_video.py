#import client libs
import gdata.youtube
import gdata.youtube.service

#YouTubeService used to generate an object so we can communicate with the YouTube API
yt_service = gdata.youtube.service.YouTubeService()

#User ID (used default as hermanmu because raw_input still does not work in ipython)
user_id = 'hermanmu'

playlist_feed = yt_service.GetYouTubePlaylistFeed(username=user_id)

print "Playlists for " + user_id + ":\n"

for playlist in playlist_feed.entry:
    print playlist.text
    #Get only the playlist ID and not the entire link
    playlistid = playlist.id.text.split('/')[-1]
    video_feed = yt_service.GetYouTubePlaylistVideoFeed(playlist_id = playlistid)
    for video in video_feed.entry:
        print "\t"+video.title.text
